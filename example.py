import numpy as np
from phd.kalman_predict_multiple import kalman_predict_multiple


model = [[1,2],[1,1]]
m_update = np.array([210, 0, 210, 0, 235, 0, 270, 0])
#.reshape(model['x_dim'], -1)
P_update = np.diag(np.array([1, 1, 1, 1, 1, 1, 1, 1]).T) ** 2
m_predict, P_predict = kalman_predict_multiple(model, m_update, P_update)

print (m_predict, " ", P_predict)