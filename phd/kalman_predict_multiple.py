import numpy as np


def kalman_predict_multiple(model=None, m=None, P=None):

    plength = m.shape[1]
    m_predict = np.zeros(m.shape)
    P_predict = np.zeros(P.reshape(-1, P.shape[1], P.shape[1]).shape)

    for idxp in range(plength):

        try:
            P.shape[2]
            m_temp, P_temp = kalman_predict_single(
                model['F'], model['Q'], m[:, idxp], P[idxp])

        except IndexError as _:

            m_temp, P_temp = kalman_predict_single(
                model['F'], model['Q'], m[:, idxp], P)

        m_predict[:, idxp] = m_temp.T
        P_predict[idxp] = P_temp

    return m_predict, P_predict


def kalman_predict_single(F=None, Q=None, m=None, P=None):

    m_predict = np.dot(F, m)
    m_predict = np.reshape(m_predict, (m.size, -1))
    P_predict = Q + np.dot(np.dot(F, P), F.T)

    return m_predict, P_predict
