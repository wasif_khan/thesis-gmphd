import numpy as np


def gaus_cap(w=None, x=None, P=None, max_number=None):

    if len(w) > max_number:
        w_new = w[:max_number]
        w_new = np.dot(w_new, (np.sum(w) / np.sum(w_new)))
        x_new = x[:, :max_number]
        P_new = P[:max_number]

    else:
        x_new = x
        P_new = P
        w_new = w

    return w_new, x_new, P_new
