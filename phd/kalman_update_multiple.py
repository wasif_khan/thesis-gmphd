import numpy as np
import numpy.matlib


def kalman_update_multiple(z=None, model=None, m=None, P=None):

    plength = m.shape[1]
    zlength = z.shape[1]

    qz_update = np.zeros((plength, zlength))
    m_update = np.zeros((plength, model['x_dim'], zlength))
    P_update = np.zeros((plength, model['x_dim'], model['x_dim']))

    for idxp in range(plength):
        qz_temp, m_temp, P_temp = kalman_update_single(
            z, model['H'], model['R'], m[:, idxp], P[idxp], model['x_dim']
        )

        qz_update[idxp] = qz_temp
        m_update[idxp] = m_temp
        P_update[idxp] = P_temp

    return qz_update, m_update, P_update


def kalman_update_single(z=None, H=None, R=None, m=None, P=None, oj=None):

    m = m.reshape(m.size, -1)
    mu = np.dot(H, m)

    S = R + np.dot(np.dot(H, P), H.T)
    Vs = np.linalg.cholesky(S)
    det_S = (np.prod(np.diag(Vs)) ** 2)
    inv_sqrt_S = np.linalg.inv(Vs)
    iS = np.dot(inv_sqrt_S, inv_sqrt_S.T)
    K = np.dot(np.dot(P, H.T), iS)

    A0 = np.dot(np.dot(-0.5, z.shape[0]), np.log(np.dot(2, np.pi)))
    A1 = np.dot(0.5, np.log(det_S))

    B0 = (z - np.tile(mu, (1, z.shape[1])))
    B1 = np.dot(iS, (z - np.tile(mu, (1, z.shape[1]))))
    B2 = np.dot(0.5, np.sum(B0.conj() * B1, axis=0))
    B2 = B2.reshape(1, B2.size)

    qz_temp = np.exp(A0 - A1 - B2)

    U0 = z - np.tile(mu, (1, z.shape[1]))
    U1 = np.tile(m, (1, z.shape[1]))

    m_temp = U1 + np.dot(K, U0)
    P_temp = np.dot((np.eye(P.shape[0], P.shape[1]) - np.dot(K, H)), P)

    return qz_temp, m_temp, P_temp
