import numpy as np
from meas.gen_observation_fn import gen_observation


def gen_meas(model=None, truth=None, noise=None):

    print('Generating measurements')
    print('Please wait...')

    meas = {}

    # variables
    meas['K'] = truth['K']
    meas['Z'] = [[] for i in range(truth['K'])]

    # generate measurements
    for k in range(truth['K']):

        if truth['N'][k] > 0:

            def get_indexes(x, xs): return [i for (
                y, i) in zip(xs, range(len(xs))) if x >= y]

            xs = np.random.random((truth['N'][k], 1))

            # detected target indices
            idx = get_indexes(model['P_D'], xs)

            # single target observations if detected
            meas['Z'][k] = gen_observation(
                model, truth['X'][k][:, idx], noise)

        # number of clutter points
        N_c = np.random.poisson(model['lambda_c'])

        A0 = np.tile(model['range_c'][0:, :1], (1, N_c))
        A1 = np.diag(np.dot(model['range_c'], np.array([-1, 1])))
        A2 = np.random.random((model['z_dim'], N_c))

        # clutter generation
        C = A0 + np.dot(A1, A2)

        # measurement is union of detections and clutter
        try:
            meas['Z'][k] = np.concatenate((meas['Z'][k], C), axis=1)
        except IndexError as _:
            pass

    print('Measurements generated')
    print('')

    return meas
