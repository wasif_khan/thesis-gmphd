import numpy as np


def gen_observation(model=None, X=None, W=None):

    # linear observation equation (position components only)
    if not W.isnumeric():
        if W is 'noise':
            W = np.dot(model['D'], np.random.normal(
                0, 1, (model['D'].shape[1], X.shape[1])))

        else:
            if W is'noiseless':
                W = np.zeros((model['D'].shape[0], X.shape[1]))

    if X == []:
        Z = []

    else:
        Z = np.dot(model['H'], X) + W

    return Z
