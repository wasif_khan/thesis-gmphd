import numpy as np


def gen_newstate_fn(model=None, Xd=None, V=None):

    # linear state space equation (CV model)
    if not V.isnumeric():
        if V is 'noise':
            V = np.dot(np.dot(model['sigma_v'], model['B']), np.random.randn(
                model['B'].shape[1], Xd.shape[1]))
        else:
            if V is 'noiseless':
                V = np.zeros((model['B'].shape[0], Xd.shape[1]))

    if Xd == []:
        X = []
    else:
        X = np.dot(model['F'], Xd) + V

    return X
