import numpy as np
from pathlib import Path
from PIL import Image
import re
from ast import literal_eval as make_tuple


def gen_truth(model=None, dataset=None):

    print('Loading data from dataset')
    print('Please wait...')

    base_dir = Path(__file__).resolve().parent.parent.parent
    img_dir_len = 'datasets/images/{}/left/'.format(dataset)
    img_dir = 'datasets/images/{}/'.format(dataset)
    anno = 'datasets/annotation/{}-annot.idl'.format(dataset)
    anno = base_dir.joinpath(anno)

    img_dir_len = base_dir.joinpath(img_dir_len)
    img_dir = base_dir.joinpath(img_dir)

    k = 0

    truth = {}

    # length of data/number of scans
    truth['K'] = len(list(img_dir_len.glob('*.png')))

    # ground truth for states of targets
    truth['X'] = [[] for i in range(truth['K'])]

    # frames
    truth['Images'] = [[] for i in range(truth['K'])]

    # ground truth for number of targets
    truth['N'] = np.zeros(truth['K'], dtype=int)

    # ground truth for labels of targets (k,i)
    truth['L'] = [[] for i in range(truth['K'])]

    # absolute index target identities (plotting)
    truth['track_list'] = [[] for i in range(truth['K'])]

    # total number of appearing tracks
    truth['total_tracks'] = 0

    truth['high'] = 0

    with open(anno, 'r') as f:
        for i in f:

            # clean annotation
            value = np.array(re.sub('[\n;"]', '', i).split(': '))

            img = np.array(Image.open(img_dir / value[0]))
            truth['Images'][k] = img

            value = np.array(make_tuple(value[1])).T
            value = np.insert(value, 1, [0], axis=0)
            value = np.insert(value, 3, [0], axis=0)
            value = np.insert(value, 5, [0], axis=0)
            value = np.insert(value, 7, [0], axis=0)

            truth['X'][k] = value
            truth['N'][k] = value.shape[1]

            if truth['N'][k] > truth['high']:

                # highest number of targets on a scene
                truth['high'] = truth['N'][k]

            k += 1

    truth['Images'] = np.array(truth['Images'])
    truth['X'] = np.array(truth['X'])

    print('Data successfully loaded')
    print('')

    return truth
